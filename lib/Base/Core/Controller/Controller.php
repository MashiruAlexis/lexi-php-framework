<?php
/**
 *	BASE_CONTROLLER
 *	@author Ramon Alexis Celis
 */

/**
 *	Becareful, This file is the base of all the created controllers and every function will be affected if
 *
 *	touched without caution.
 */

Class BASE_CORE_CONTROLLER {

	/**
	 * Default View
	 */
	public $pathToView = array();

	/**
	 *	Path To View
	 */
	public $pathView;

	/**
	 *	Page Title
	 */
	public $pageTitle;

	/**
	 *	File Extensions
	 */
	public $fileExt = ["PHP" => ".php", "CSS" => ".css", "JS" => ".js", "MIN" => ".min"];

	/**
	 *	Css Single Controller
	 */
	public $cssFiles = array();

	/**
	 *	JS Single Controller
	 */
	public $jsFiles = array();

	/**
	 *	Capture all blocks
	 */
	public $block = array();

	/**
	 *	Default Header File
	 */
	public $headerFile = "header.phtml";

	/**
	 *	Get jsFiles Value
	 */
	public function getJsFiles() {
		foreach($this->jsFiles as $js) {
			echo $js;
		}
	}

	/**
	 *	Get CSS Files
	 */
	public function getCssFiles() {
		foreach($this->cssFiles as $css) {
			echo $css;
		}
	}

	/**
	 *	Load Master CSS
	 */
	public function getBaseCSS() {
		$baseCSS = glob($this->getBasePath(["modName" => null, "file" => null, "modType" => "BASE_CSS"]));
		foreach ($baseCSS as $value) {
			$this->cssFiles[] = '<link rel="stylesheet" href="' . BASE_URL . str_replace(ROOT, "", $value ).'" type="text/css">' . "\n";
		}
	}

	/**
	 *	Load Master JS
	 */
	public function getBaseJS() {
		$baseJS = glob($this->getBasePath(["modName" => null, "file" => null, "modType" => "BASE_JS"]));
		foreach ($baseJS as $value) {
			$this->jsFiles[] = '<script src="' . BASE_URL . str_replace(ROOT, "", $value ).'"></script>' . "\n";
		}
	}

	/**
	 *	Load Master IMAGE
	 */
	public function getIMG() {
		return $this->getBasePath(["modName" => null, "file" => null, "modType" => "BASE_IMG"]);
	}

	/**
	 *	Load Single JS file
	 */
	public function getModuleJS($js) {
		$js = glob($this->getPath(["modName" => $js, "file" => $js, "modType" => "JavaScript"]) . "*.js");
		foreach ($js as $value) {
			$this->jsFiles[] =  "<script src='" . BASE_URL . str_replace(ROOT, "", $value). "'></script>" . "\n";
		}
	}

	/**
	 *	Load Module CSS [load all the css on the specified module]
	 */
	public function getModuleCSS($css) {
		$css = explode("/", $css);
		$stockCss = $css[0];
		$css = glob($this->getPath(["modName" => $css[0], "file" => null, "modType" => "moduleCss" ]) . "*.css");
		foreach ($css as $value) {
			$this->cssFiles[] = '<link rel="stylesheet" href="' . BASE_URL . str_replace(ROOT, "", $value ).'" type="text/css">' . "\n";
		}
	}

	/**
	 * Load a single css file
	 */
	public function getCSS($css) {
		$css = explode("/", $css);
		$this->cssFiles[] = $css[1];
		$css[1] = $this->removeExtFile($css[1]);
		$css[1] = str_replace(ROOT, "", $this->getPath(["modName" => $css[0], "file" => $css[1], "modType" => "css"]));
		echo '<link rel="stylesheet" href="' . BASE_URL . DS . $css[1] . '" type="text/css">' . "\n";
	}

	/**
	 * Remove the extension string from a variable
	 */
	public function removeExtFile($file) {
		foreach ($this->fileExt as $value) {
			$file = str_replace($value, "", $file);
		}
		return $file;
	}

	/**
	 *	Globaly set a page title
	 */
	public function setPageTitle($title) {
		$this->pageTitle = $title;
	}

	/**
	 *	Get the Page Title is it was Set
	 */
	public function getPageTitle() {
		echo $this->pageTitle;
	}

	/**
	 *	Get a View fron a certain module
	 */
	public function getView($view) {
		$view = explode("/", $view);
		return $this->getPath(["modName" => $view[0],"file"=>$view[1] ,"modType" => "View"]);
	}

	/**
	 *	Get a model from a certain module
	 */
	public function getModel($model) {
		$model = explode("/", $model);
		return $this->getPath(["modName" => $model[0], "file" => $model[1], "modType" => "Model"]);
	}

	/**
	 *	Get a controller from a certain module
	 */
	public function getController($controller) {
		$controller = explode("/", $controller);
		return $this->getPath(["modName" => $controller[0], "file" => $controller[1], "modType" => "Controller"]);
	}

	/**
	 *	Get a path from a certain file
	 */
	public function getPath($pathData = array()) {

		$_PathLoader = array_filter(glob(ROOT . DIR_APP . "*"), "is_dir");
		foreach ($_PathLoader as $value) {
			$dirModule[] = str_replace(ROOT . DIR_APP, "", $value);
			$this->pathToView[str_replace(ROOT . DIR_APP, "", $value)] = $value . DS;
		}

		if(in_array($pathData["modName"], $dirModule)) {
			switch ($pathData["modType"]) {
				case "View":
					return $this->pathToView[$pathData["modName"]] . $pathData["modType"] . DS . $pathData["file"] . ".phtml";
					break;
				case "Controller":
					return include $this->pathToView[$pathData["modName"]] . $pathData["modType"] . DS . $pathData["file"] . ".php";
					break;
				case "Model":
					return include $this->pathToView[$pathData["modName"]] . $pathData["modType"] . DS . $pathData["file"] . ".php";
					break;
				case "css":
					return $this->pathToView[$pathData["modName"]] . DIR_ETC . $pathData["modType"] . DS . $pathData["file"] . ".css";
					break;
				case "moduleCss":
					return $this->pathToView[$pathData["modName"]] . DIR_ETC . DIR_CSS . DS;
					break;
				case "JavaScript":
					return $this->pathToView[$pathData["modName"]] . DIR_ETC . DIR_JS . DS;
					break;
				default:
					return "The file your trying to invoke doesnt exist.";
			}
		}
	}

	/**
	 * Get the path from the base files
	 */
	public function getBasePath($pathBaseData = array()) {

		switch ($pathBaseData['modType']) {
			case "BASE_CSS":
				return ROOT . DIR_LIBRARY . DIR_BASE . DIR_CORE_CONTROLLER . DIR_ETC . DIR_CSS . DS . "*.css";
				break;
			case "BASE_JS":
				return ROOT . DIR_LIBRARY . DIR_BASE . DIR_CORE_CONTROLLER . DIR_ETC . DIR_JS . DS . "*.js";
				break;
			case "BASE_IMG":
				return BASE_URL . DIR_LIBRARY . DIR_BASE . DIR_CORE_CONTROLLER . DIR_ETC . DIR_IMG;
				break;

			default:
				return false;
				break;
		}
	}

	/**
	 *	Set Block
	 */
	public function setBlock($view) {
		$view = explode("/", $view);
		$this->block[] = $this->getPath(["modName" => $view[0],"file"=>$view[1] ,"modType" => "View"]);
	}

	/**
	 *	Get Blocks
	 */
	public function getBlock() {
		foreach($this->block as $block) {
			include $block;
		}
	}

	/**
	 *	Load Defauilt Layout
	 */
	public function loadLayout() {
		$fileLoc = ROOT . DIR_LIBRARY . DIR_BASE . DIR_CORE_CONTROLLER . "View" . DS . $this->headerFile;
		if(file_exists($fileLoc)) {
			$this->getBaseCss();
			$this->getBaseJs();
			return include $fileLoc;
		}else{
			echo "not Found" . $fileLoc;
		}
	}


	/**
	 *	Test if a module was integrated successfully to CloudMax
	 */
	public function testControllerState($varState = null) {
		if(isset($varState)){
			echo "<br/> " . $varState . " is good to go! code now buddy! <br/>";
		}else{
			echo "<br/> If you can read this message means that the controller is working fine.<br/>";
		}
		
	}


}