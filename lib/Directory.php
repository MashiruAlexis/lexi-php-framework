<?php
/**
 *	DIRECTORY
 *	@var English
 *	@author Ramon Alexis Celis
 */

/**
 *	Automatically get the Separator
 */

define("DS", DIRECTORY_SEPARATOR);

/**
 *	This is the directory variables
 */
define("ROOT", $_SERVER['DOCUMENT_ROOT'] . DS);

define("DIR_APP", "app" . DS);

define("DIR_CONTROLLER", "Controller" . DS);

define("DIR_LIBRARY", "lib" . DS);

define("DIR_CORE_CONTROLLER", "Core" . DS );

define("DIR_BASE", "Base" . DS);

define("DIR_ETC", "etc" . DS);

define("DIR_CSS", "css" , DS);

define("DIR_JS", "js");

define("DIR_IMG", "img" . DS);



