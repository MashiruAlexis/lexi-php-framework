<?php
/**
 *	Boot the Site
 *
 */

Class Boot {

	/**
	 * Library Directory
	 */
	public $libDir 		= "lib/";

	/**
	 * Directory Variables
	 */
	public $directory 	= "Directory";

	/**
	 *	Default Language
	 */
	public $language 	= "English";

	/**
	 *	Constant Variables
	 */
	public $consVar 	= "Contant";

	/**
	 *	Congfiguration
	 */
	public $config 		= "Config";

	/**
	 *	Site Extension
	 */
	protected $file 	= ".php";

	/**
	 *	Boot State
	 */
	public $state 		= array();

	/**
	 *	App Directory
	 */
	public $dirApp 	= "app/";

	/**
	 *	Core Variable
	 */
	protected $varCore	= "Core";

	/**
	 *	Base Controller Variable
	 */
	protected $baseCoreController;


	function __construct() {

		$this->loadDirectoryVar();
		$this->loadConsVar();
		$this->loadConfiguration();
		$this->loadDefaultLanguage();
		$this->loadCloudMaxCore();

	}

	function loadConsVar() {

		if(file_exists($this->libDir . $this->consVar . $this->file)) {
			require $this->libDir . $this->consVar . $this->file;
		}else{
			$this->state[] = "Failed to load Contant";
		}
	}

	function loadDirectoryVar() {

		if(file_exists($this->libDir . $this->directory . $this->file)) {
			require $this->libDir . $this->directory . $this->file;
		}else{
			$this->state[] = "Failed to load Directory";
		}
	}

	function loadConfiguration() {

		if(file_exists($this->libDir . $this->config . $this->file)) {
			require $this->libDir . $this->config . $this->file;
		}else{
			$this->state[] = "Failed to load Configuration";
		}
	}

	function loadDefaultLanguage() {

		if(file_exists($this->libDir . $this->language . $this->file)) {
			require $this->libDir . $this->language . $this->file;
		}else{
			$this->state[] = "Failed to load Language";
		}
	}

	public static function loadAppDir() {

		return $this->dirApp;
	}

	function loadCloudMaxCore() {

		$this->baseCoreController = ROOT . DIR_LIBRARY . DIR_BASE . DIR_CORE_CONTROLLER . DIR_CONTROLLER;
		
		if(file_exists($this->baseCoreController . CONTROLLER . $this->file)) {
			require_once $this->baseCoreController . CONTROLLER . $this->file;
			$coreInstance = rtrim(str_replace($this->file, "", str_replace(DS, "_", str_replace(ROOT . DIR_LIBRARY, "", $this->baseCoreController))), "_");
		}else{
			$this->state[] = "Cloudmax Main Controller failed to initialize properly";
		}

		if(file_exists($this->dirApp . $this->varCore . $this->file)){
			include $this->dirApp . $this->varCore . $this->file;
			$_core = new $this->varCore;
		}else{
			$this->state[] = "CloudMax Core failed to initialize properly";
		}
	}
}
