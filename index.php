<?php
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * CloudMax
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to admin@cloudmax.xyz so we can send you a copy immediately.
 *
 * @category    BOOT
 * @package     Index Loader
 * @copyright  	Copyright (c) 2015 (http://www.cloudmax.xyz)
 * @license    	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
/**
 *			CloudMax Team
 *
 * @author      Ramon Alexis Celis <admin@cloudmax.xyz>
 * @author      Girly Saac
 * @author      Ralph Tillada
 * @author      Andrew Tagadiad
 * @author      Daniel Pacadaljen
 * @author      Kurt Navaro
 * @author      Kirby Duran
 * @version 	1.0.0
 *
 **/
print_r($_GET); exit();
/**
 * Cludmax Boot Sequence
 *
 * @category    BootMode
 * @package     Index
 * @author      Ramon Alexis Celis <admin@cloudmax.xyz> */
   require_once "init.php";
/*
 **/

$_CloudMax = new Boot;

/**
 *	Check if there are error during the startup
 *
 */
if(isset($_init->state)){
	foreach ($_init->state as $value) {
		echo $value . "<br/>";
	}
}