<?php
/**
 * 	Default module
 *	@author mashirualexis
 */

Class INDEX_CONTROLLER extends BASE_CORE_CONTROLLER {

	public function __construct() {
		$this->getModuleCss("index");
		$this->getModuleJs("index");
	}

	public function index() {



		$this->setPageTitle("Magento");
		$this->setBlock("index/index");

		$this->setBlock("index/nav");
		$this->setBlock("index/index");
		$this->loadLayout();

		// $this->getView("index/index");
	}
}