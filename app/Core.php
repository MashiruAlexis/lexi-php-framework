<?php
/**
 *	Core of CloudMax
 *
 */

/**
 *	Please be reminded that this file is very important and highly recomemded DO NOT EDIT
 *	
 *	unless you know what your doing :D
 *
 * @author Ramon Alexis Celis
 */

Class Core {

	/**
	 * Default Controller [this is use when no module is selected]
	 */
	protected $controller = "index";

	/**
	 * Default Method [This method is called in the controller]
	 */
	protected $method = "index";

	/**
	 *	Parameters [This are the details passed down (this act like the $_GET[] function of the system)]
	 */
	protected $params = [];

	/**
	 *	Current Controller Path [the path of the controller above]
	 */
	protected $currentControllerPath;

	/**
	 *	File [The file type of the base controllers]
	 */
	public $file = ".php";

	/**
	 * Directory of CloudMax Core file
	 */
	public $dirCloudCore = "Cloudmax/";

	/**
	 * Url [All the url are access via this variable]
	 */
	public $url;

	/**
	 *	Path to Controllers
	 */
	public $pathControllers = array();

	/**
	 * Default [The code in this method will automatically run after the core is instantiated]
	 */
	public function __construct() {

		$this->url = $this->parseUrl();
		$this->loadController(strtolower($this->url[0]));

		if(isset($this->url[1])) {
			$this->loadMethod($this->url[1]);
		}

		if(isset($this->url)) {
			$this->params = array_values($this->url);
		}

		if(method_exists($this->controller, $this->method)) {
			call_user_func_array([$this->controller, $this->method], $this->params);
		}
		
	}

	/**
	 * Load Method [This will call the method passed in the url]
	 */
	function loadMethod($methodName) {
		if(method_exists($this->controller, $methodName)){			
			$this->method = $methodName;
			unset($this->url[1]);
		}
	}

	/**
	 *	Load Controller [This will load the defined controller]
	 */
	function loadController($controllerName) {
		$_PathLoader = array_filter(glob(ROOT . DIR_APP . "*"), "is_dir");
		foreach ($_PathLoader as $value) {
			$dirControllers[] = str_replace(ROOT . DIR_APP, "", $value);
			$this->pathControllers[str_replace(ROOT . DIR_APP, "", $value)] = $value . DS;
		}
		if(in_array($controllerName, $dirControllers)) {

			$this->currentControllerPath = $this->pathControllers[$controllerName] . DIR_CONTROLLER . $controllerName . ".php";

			if(file_exists($this->currentControllerPath)) {
				$this->controller = $controllerName;
				require $this->currentControllerPath;
				$this->controller = $this->getClassName($this->currentControllerPath);
				$this->controller = new $this->controller;
				$this->controller->pathToControllers = $this->pathControllers;
				unset($this->url[0]);
			}else{
				echo "Please define a controller.";
			}
		}else{

			if(!isset($this->url)){
				$this->loadDefaultController();
			}else{
				echo "Page not Found!";
			}
		}
	}

	/**
	 *	Load Default Controller [This will be fired if there are no controller]
	 */
	public function loadDefaultController() {
		$this->currentControllerPath = ROOT . DIR_APP . $this->controller . DS . DIR_CONTROLLER . $this->controller . $this->file;
		if(file_exists($this->currentControllerPath)) {
			require_once $this->currentControllerPath;
			$this->controller = $this->getClassName($this->currentControllerPath);
			$this->controller = new $this->controller;
		}else{
			echo "Default Controller not found. <br/>";
			echo $this->currentControllerPath;
		}
	}

	/**
	 * Get Class Name [This will get the class base on the path of the controller]
	 */
	public function getClassName($class) {
		return $class = str_replace( DS, "_", rtrim(str_replace($this->controller . $this->file, "", str_replace(DIR_APP, "", str_replace(ROOT, "", $class))), DS));
	}

	/**
	 *	Parse Url [This will parse the url from the browser]
	 */
	public function parseUrl() {
		if(isset($_GET['url'])) {
			return $this->chkUrl($_GET['url']);
		}
	}

	/**
	 * Check Url [Shortcut way of checking if the url is valid]
	 */
	public function chkUrl($url) {
		return $url = explode("/", filter_var(rtrim($url, "/"), FILTER_SANITIZE_URL));
	}

	/**
	 *	Deep Check Url [Same with chkUrl this is the long version]
	 */
	public function deepChkUrl($varUrl) {
		$varUrl = rtrim($varUrl, "/");
		$varUrl = filter_var($varUrl, FILTER_SANITIZE_URL);
		$varUrl = explode("/", $varUrl);
		return $varUrl;

	}

	/**
	 *	Logger
	 */
	public static function log($val) {
		echo "<pre>";
		print_r($val);
		echo "</pre>";
	}
	
}