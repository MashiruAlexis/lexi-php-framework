<?php

/**
 *	Login Controller
 *	@author Ramon Alexis Celis
 */

Class LOGIN_CONTROLLER extends BASE_CORE_CONTROLLER {

	public function index() {
		$this->getView("login/login");
	}
}