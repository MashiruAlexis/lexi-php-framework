<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Login to MyOtakuSpace were you can express your otakusness to the extreme.">
    <meta name="author" content="MyOtakuSpace">
    <title>MyOtakuSpace - Login</title>
    <?php $this->getBaseCss(); ?>
</head>
<body class="login">
    <div id="content">
        <div class="container-fluid">
            <div class="lock-container">
                <h1>Account Access</h1>
                <div class="panel panel-default text-center">
                    <img src="<?php echo $this->getIMG(); ?>people/110/guy-5.jpg" class="img-circle">
                    <div class="panel-body">
                        <input class="form-control" type="text" placeholder="Username">
                        <input class="form-control" type="password" placeholder="Enter Password">
                        <a href="index.html" class="btn btn-primary">Login <i class="fa fa-fw fa-unlock-alt"></i></a>
                        <a href="#" class="forgot-password">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $this->getJS(); ?>