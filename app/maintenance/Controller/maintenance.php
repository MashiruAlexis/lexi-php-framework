<?php

Class MAINTENANCE_CONTROLLER extends BASE_CORE_CONTROLLER {

	function index() {
		$this->setPageTitle("Maintenance");
		$this->getView("maintenance/index");
	}
}